// Sample Functions to Test

function getCircleArea(radius){
	let answer = 3.1416*(radius**2)
	let roundOff = answer.toFixed(2)
	return JSON.parse(roundOff)
}

function checkIfPassed(score, total){ 
	return (score/total)*100 >= 75;
}

function getAverage(num1, num2, num3, num4){

	return (num1 + num2 + num3 + num4) / 4;
}

function getSum(num1, num2){

	return num1 + num2;
}

function getDifference(num1, num2){

	return num1 - num2;
}

const factorial = (n) => {

	if(typeof n !== "number") return undefined;
	if( n < 0 ) return undefined;
	if(n === 0) return 1;
	if(n === 1) return 1;

	return n * factorial(n-1);
}

// Activity 

const div_check = (n) => {

	if(n % 5 === 0) return true
	if(n % 7 === 0) return true
	if(n % 5 === 0 || n % 7 === 0) return true
	if(n % 5 !== 0 || n % 7 !== 0) return false
	
}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	factorial: factorial,
	div_check: div_check
}

// npm test -- -f average

